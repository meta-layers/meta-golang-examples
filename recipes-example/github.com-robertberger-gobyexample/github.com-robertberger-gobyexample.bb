DESCRIPTION = "The Go Mod Example Project"

SRC_URI = "git://github.com/robertberger/gobyexample.git;protocol=https;"
SRCREV = "${AUTOREV}"
LICENSE = "CC-BY-3.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/${PN}-${PV}/src/${GO_IMPORT}/LICENSE;md5=e2f628e8e3b08d8f45d1444f3e4e5224"
inherit go
GO_IMPORT = "github.com/robertberger/gobyexample"
