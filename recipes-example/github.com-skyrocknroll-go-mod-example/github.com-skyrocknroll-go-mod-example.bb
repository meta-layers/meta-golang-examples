DESCRIPTION = "The Go Mod Example Project"

SRC_URI = "git://github.com/skyrocknroll/go-mod-example.git;protocol=https;"
SRCREV = "${AUTOREV}"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/${PN}-${PV}/src/${GO_IMPORT}/LICENSE;md5=86d3f3a95c324c9479bd8986968f4327"
#DEPENDS += "golang.org-x"
inherit go
GO_IMPORT = "github.com/skyrocknroll/go-mod-example"

DEPENDS += "go-dep-native"
RDEPENDS_${PN}-staticdev += "bash"

# this works but...
# 1) where do go.mod and go.sum come from?
# 1.1) shouldn't they be created by go mod init?
# 2) what about licenses of all the modules included? We only deal with the top level licenses here.

do_compile:prepend() {
    rm -f ${WORKDIR}/build/src/${GO_IMPORT}/Gopkg.toml
    rm -f ${WORKDIR}/build/src/${GO_IMPORT}/Gopkg.lock
    cd ${WORKDIR}/build/src/${GO_IMPORT}
    dep init
    dep ensure
}

