DESCRIPTION = "The mattn SQLite3 Go Library"
GO_IMPORT = "github.com/mattn/go-sqlite3"
inherit go
DEPENDS="sqlite3"
SRC_URI = "git://github.com/mattn/go-sqlite3.git;protocol=https;"
SRCREV = "${AUTOREV}"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${S}/src/${GO_IMPORT}/LICENSE;md5=2b7590a6661bc1940f50329c495898c6"
CGO_ENABLED = "1"
