some info:
  https://stackoverflow.com/questions/48318030/yocto-recipe-to-manage-go-dependencies-with-dep-tool

demonstrates:
  go dep
  go.mod support? - is in remote repo
  go.sum support? - is in remote repo


$ oe-pkgdata-util list-pkgs github.com-skyrocknroll-go-mod-example*
github.com-skyrocknroll-go-mod-example-dev
github.com-skyrocknroll-go-mod-example-staticdev
github.com-skyrocknroll-go-mod-example-dbg
github.com-skyrocknroll-go-mod-example

$ oe-pkgdata-util list-pkg-files github.com-skyrocknroll-go-mod-example
github.com-skyrocknroll-go-mod-example:
        /usr/bin/go-mod-example

oe-pkgdata-util list-pkg-files github.com-skyrocknroll-go-mod-example-dev
github.com-skyrocknroll-go-mod-example-dev:
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/Gopkg.lock
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/Gopkg.toml
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/LICENSE
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/README.md
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/LICENSE
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/README.md
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/doc.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/exec.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/funcs.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/go.mod
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/helper.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/parse/lex.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/parse/node.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/parse/parse.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/template/template.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/units/COPYING
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/units/README.md
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/units/bytes.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/units/doc.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/units/go.mod
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/units/go.sum
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/units/si.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/alecthomas/units/util.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/AUTHORS
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/LICENSE
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/README.md
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/doc.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/go.mod
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/middleware.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/mux.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/regexp.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/route.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/gorilla/mux/test_helpers.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/konsorten/go-windows-terminal-sequences/LICENSE
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/konsorten/go-windows-terminal-sequences/README.md
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/konsorten/go-windows-terminal-sequences/go.mod
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/konsorten/go-windows-terminal-sequences/sequences.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/konsorten/go-windows-terminal-sequences/sequences_dummy.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/.travis.yml
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/CHANGELOG.md
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/LICENSE
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/README.md
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/alt_exit.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/appveyor.yml
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/doc.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/entry.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/exported.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/formatter.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/go.mod
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/go.sum
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/hooks.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/json_formatter.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/logger.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/logrus.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/terminal_check_appengine.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/terminal_check_bsd.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/terminal_check_no_terminal.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/terminal_check_notappengine.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/terminal_check_solaris.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/terminal_check_unix.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/terminal_check_windows.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/text_formatter.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/github.com/sirupsen/logrus/writer.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/AUTHORS
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/CONTRIBUTORS
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/LICENSE
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/PATENTS
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/unix/README.md
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/unix/affinity_linux.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/unix/aliases.go
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/unix/asm_aix_ppc64.s
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/unix/asm_darwin_386.s
        /usr/lib/go/src/github.com/skyrocknroll/go-mod-example/_vendor-20200319190124/golang.org/x/sys/unix/asm_darwin_amd64.s

...



e-pkgdata-util list-pkg-files github.com-skyrocknroll-go-mod-example-staticdev
github.com-skyrocknroll-go-mod-example-staticdev:
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/LICENSE
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/README.md
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/doc.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/example_test.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/examplefiles_test.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/examplefunc_test.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/exec.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/exec_test.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/funcs.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/go.mod
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/helper.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/multi_test.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/parse/lex.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/parse/lex_test.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/parse/node.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/parse/parse.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/parse/parse_test.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/template.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/testdata/file1.tmpl
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/testdata/file2.tmpl
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/testdata/tmpl1.tmpl
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-template/testdata/tmpl2.tmpl
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-units/COPYING
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-units/README.md
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-units/bytes.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-units/bytes_test.go
        /usr/lib/go/pkg/dep/sources/https---github.com-alecthomas-units/doc.go

...

$ oe-pkgdata-util list-pkg-files github.com-skyrocknroll-go-mod-example-dbg
github.com-skyrocknroll-go-mod-example-dbg:
        /usr/lib/debug/usr/bin/go-mod-example.debug

$ oe-pkgdata-util list-pkg-files github.com-skyrocknroll-go-mod-example-dbg
github.com-skyrocknroll-go-mod-example-dbg:
        /usr/lib/debug/usr/bin/go-mod-example.debug

----
