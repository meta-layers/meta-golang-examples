DESCRIPTION = "Delve"
SUMMARY = "Gccgo has native gdb support.\
Note that Delve is a better alternative to GDB when debugging Go programs built with the standard toolchain. \
It understands the Go runtime, data structures, and expressions better than GDB. \
Delve currently supports Linux, OSX, and Windows on amd64. \
"
HOMEPAGE = "https://github.com/go-delve/delve/"

# if you want to debug with the -native build try this:
# bitbake github.com-go-delve-delve-native -c addto_recipe_sysroot
# oe-run-native github.com-go-delve-delve-native dlv

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE;md5=7de91db1ddb3397f38ac26ef1beb2e62"

GO_IMPORT = "github.com/gravitational/teleport"

# don't check out modules read only - se we can clean/cleanall
#GOBUILDFLAGS_append = " -modcacherw"
#inherit go go-mod
inherit go

#PTEST_ENABLE="0"

BRANCH = "nobranch=1"
SRCREV = "2cb40abd8ea8fb2915304ea4888b5b9f3e5bc223"

SRC_URI = "\
           git://github.com/gravitational/teleport;${BRANCH};protocol=https;destsuffix=${BPN}-${PV}/src/${GO_IMPORT} \
           file://0001-use-dollar-go-instead-of-go.patch \
          "
# We need go-native for ${GO} underneath
# I thought that "inherit go" would take care about it
# but apparently it does not
#do_compile[depends] += "go-native:do_populate_sysroot"
# trying license check in devshell
#do_devshell[depends] += "github.com-google-go-licenses-native:do_populate_sysroot"

export CGO_ENABLED="1"
#export CGO_ENABLED="0"

WHAT_TO_BUILD="github.com/gravitational/teleport"
# custom do_compile:
# 	1. only fetch 
# 	2. fix permissions of pkg/mod/xxx directories
# 	3. modifed version of do_compile, taken from bbclass
do_compile() {
	# let's try not to build, but just download the stuff
	# go -d does not build, but just download
	${GO} get -d ${WHAT_TO_BUILD}
	# --> fix permissions
#    	if [ -d ${B}/pkg/mod ]; then
#           pushd ${B}/pkg/mod
#           for directory in * ; do
#             #bbwarn "find ${B}/pkg/mod -depth -type d -name ${directory} -exec chmod -R 755 {} \;"
#             find ${B}/pkg/mod -depth -type d -name ${directory} -exec chmod -R 755 {} \;
#           done
#           popd
#        fi
	# <-- fix permissions	

	# --> this is a modified version of do_compile from the bbclass
#	export TMPDIR="${GOTMPDIR}"
#        if [ -n "${GO_INSTALL}" ]; then
#                if [ -n "${GO_LINKSHARED}" ]; then
#                        ${GO} install ${GOBUILDFLAGS} ${WHAT_TO_BUILD}
#                        rm -rf ${B}/bin
#                fi
#                ${GO} install ${GO_LINKSHARED} ${GOBUILDFLAGS} ${WHAT_TO_BUILD}
#        fi
	# <-- this is a modified version of do_compile from the bbclass
}

# --> fix qa issues
#RDEPENDS_${PN}-staticdev += "\
#                       bash \
#                       "
#RDEPENDS_${PN}-dev += "\
#                       bash \
#                       "
#INSANE_SKIP_${PN}-dev += "ldflags"
# <-- fix qa issues

# we want also those:
#BBCLASSEXTEND="native nativesdk"
