# add this to the go recipe you want to check:

# trying license check in devshell
do_devshell[depends] += "github.com-google-go-license-detector-native:do_populate_sysroot github.com-google-go-licenses-native:do_populate_sysroot"

# then:

bitbake github.com-influxdata-telegraf -c devshell

# in devshell:

cd src/github.com/influxdata/telegraf/

go mod vendor

cd vendor

license-detector $(cat modules.txt | grep "^#" | cut -d' ' -f2)

you'll get something like this:

cloud.google.com/go
        99%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
        85%     SHL-0.5
explicit
        could not clone repo from explicit: repository not found
cloud.google.com/go/bigquery
        99%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
        85%     SHL-0.5
explicit
        could not clone repo from explicit: repository not found
cloud.google.com/go/pubsub
        99%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
        85%     SHL-0.5
explicit
        could not clone repo from explicit: repository not found
code.cloudfoundry.org/clock
        98%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
        85%     SHL-0.5
explicit
        could not clone repo from explicit: repository not found
collectd.org
        95%     0BSD
explicit
        could not clone repo from explicit: repository not found
github.com/Azure/azure-amqp-common-go/v3
        95%     MIT
        82%     MIT-0
github.com/Azure/azure-event-hubs-go/v3
        95%     MIT
        82%     MIT-0
explicit
        could not clone repo from explicit: repository not found
github.com/Azure/azure-pipeline-go
        94%     MIT
        81%     MIT-0
github.com/Azure/azure-sdk-for-go
        99%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
...

and so on




