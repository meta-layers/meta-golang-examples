#SUMMARY = "Meta package for building a installable Go toolchain"
#LICENSE = "MIT"

#inherit populate_sdk

#TOOLCHAIN_HOST_TASK:append = " \
#    packagegroup-go-cross-canadian-${MACHINE} \
#"

# trying to add to host/cross sdk:
TOOLCHAIN_HOST_TASK:append = " \
     nativesdk-github.com-go-delve-delve \
"

#TOOLCHAIN_TARGET_TASK:append = " \
#    ${@multilib_pkg_extend(d, 'packagegroup-go-sdk-target')} \
#"
