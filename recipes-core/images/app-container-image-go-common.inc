# Note that busybox is required to satify /bin/sh requirement of lghttpd,
# and the access* modules need to be explicitly specified since RECOMMENDATIONS
# are disabled.
IMAGE_INSTALL += " \
	busybox \
	go-helloworld \
"

# go-helloworld:
#        /usr/bin/go-helloworld

# github.com-skyrocknroll-go-mod-example:
#        /usr/bin/go-mod-example

