DESCRIPTION = "go-licenses"
SUMMARY = "go-licenses analyzes the dependency tree of a Go package/binary. \
           It can output a report on the libraries used and under what license they can be used. \
           It can also collect all of the license documents, copyright notices and source code into a directory \
           in order to comply with license terms on redistribution."
HOMEPAGE = "https://github.com/google/go-licenses"

########### I give this up for now ###########
# This is how I got it to run in a (hacked) docker container:
#      https://github.com/google/go-licenses/issues/56
# Don't see yet how useful it could be as a tool here

## bitbake github.com-google-go-licenses-native -c cleansstate
# bitbake github.com-google-go-licenses-native
# figure out where the build dir is to find the executable:
# bitbake github.com-google-go-licenses-native -e | grep ^B=
# ${BUILDDIR}/bin/go-licenses 
# bitbake github.com-google-go-licenses-native -c devshell
# 
#
# bitbake github.com-google-go-licenses-native -c addto_recipe_sysroot
# oe-run-native github.com-google-go-licenses-native go-licenses -h

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

GO_IMPORT = "github.com/google/go-licenses"

# don't check out modules read only - se we can clean/cleanall
GOBUILDFLAGS:append = " -modcacherw"
inherit go go-mod

BRANCH = "nobranch=1"
SRCREV = "ce1d9163b77d8eab53ba87e675dc78bb60251420"

SRC_URI = "\
           git://github.com/google/go-licenses;${BRANCH};protocol=https;destsuffix=${BPN}-${PV}/src/${GO_IMPORT} \
          "
