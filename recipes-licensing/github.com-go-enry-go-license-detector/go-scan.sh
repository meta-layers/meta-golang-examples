#!/bin/sh

HERE=$(pwd)

# let's find the top level directories to scan, but exclude cache
DIRS="$(find . -maxdepth 1 -mindepth 1 -type d -exec basename {} \; | grep -v cache)"
echo "DIRS: ${DIRS}"

# let's go trough the top level directories
for directory in ${DIRS}; do
    if [ -d $directory ]; then
       pushd ${directory}
       echo "== ${directory} =="
       set -x
       # remove from previous run
       rm -f license-${directory}.txt
       # generate a file for each top level dir
       find . -type d -exec license-detector {} \; | tee -a license-${directory}.txt
       # copy the result ${HERE}
       cp license-${directory}.txt ${HERE}
       set +x
       popd
    fi
done
