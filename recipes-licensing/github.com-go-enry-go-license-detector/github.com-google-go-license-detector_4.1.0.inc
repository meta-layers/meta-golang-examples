DESCRIPTION = "license-detector"
SUMMARY = "Project license detector - a command line application and a library, written in Go. \
           It scans the given directory for license files, normalizes and hashes them and outputs all the fuzzy matches with the list of reference texts. \
           The returned names follow SPDX standard."
HOMEPAGE = "https://github.com/go-enry/go-license-detector"

# see readme how to use it

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE.md;md5=e06bb9eb867ad187ff551478be6d9747"

GO_IMPORT = "github.com/go-enry/go-license-detector"

# don't check out modules read only - se we can clean/cleanall
GOBUILDFLAGS:append = " -modcacherw"
inherit go go-mod

BRANCH = "nobranch=1"
SRCREV = "4c9588dc6beb5109dcffd5abba20ea6ff3f85bc0"

SRC_URI = "\
           git://github.com/go-enry/go-license-detector;${BRANCH};protocol=https;destsuffix=${BPN}-${PV}/src/${GO_IMPORT} \
          "
#BBCLASSEXTEND="native nativesdk"
