# add this to the go recipe you want to check:

# trying license check in devshell
do_devshell[depends] += "github.com-google-go-license-detector-native:do_populate_sysroot"

# then:

bitbake github.com-influxdata-telegraf

bitbake github.com-influxdata-telegraf -c devshell

# in devshell:

cd ../build/pkg/mod/

cp /workdir/go-scan.sh .

chmod +x go-scan.sh

sh-4.4# ./go-scan.sh
DIRS: honnef.co
go.opencensus.io@v0.22.3
golang.zx2c4.com
gopkg.in
k8s.io
gotest.tools@v2.2.0+incompatible
go.starlark.net@v0.0.0-20200901195727-6e684ef5eeee
github.com
golang.org
cloud.google.com
google.golang.org
code.cloudfoundry.org
modernc.org
collectd.org@v0.3.0
/workdir/build/container-x86-64-golang-master/tmp/work/core2-64-resy-linux/github.com-influxdata-telegraf/1.18.0-r0/build/pkg/mod/honnef.co /workdir/build/container-x86-64-golang-master/tmp/work/core2-64-resy-linux/github.com-influxdata-telegraf/1.18.0-r0/build/pkg/mod
== honnef.co ==
+ rm -f license-honnef.co.txt
+ find . -type d -exec license-detector '{}' ';'
+ tee -a license-honnef.co.txt
.
        no license file was found
./go
        no license file was found
./go/tools@v0.0.1-2020.1.3
        100%    Apache-2.0
        93%     MIT
        81%     MIT-0
./go/tools@v0.0.1-2020.1.3/images
        no license file was found
./go/tools@v0.0.1-2020.1.3/images/screenshots
        no license file was found
./go/tools@v0.0.1-2020.1.3/callgraph
        no license file was found
./go/tools@v0.0.1-2020.1.3/callgraph/rta
        no license file was found
./go/tools@v0.0.1-2020.1.3/callgraph/rta/testdata
        no license file was found
...





#cd src/github.com/influxdata/telegraf/cmd/telegraf

# not sure if it helps to remove the .go files which contain main but are not used

#go mod vendor

#cd vendor

license-detector $(cat modules.txt | grep "^#" | cut -d' ' -f2)

you'll get something like this:

cloud.google.com/go
        99%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
        85%     SHL-0.5
explicit
        could not clone repo from explicit: repository not found
cloud.google.com/go/bigquery
        99%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
        85%     SHL-0.5
explicit
        could not clone repo from explicit: repository not found
cloud.google.com/go/pubsub
        99%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
        85%     SHL-0.5
explicit
        could not clone repo from explicit: repository not found
code.cloudfoundry.org/clock
        98%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
        85%     SHL-0.5
explicit
        could not clone repo from explicit: repository not found
collectd.org
        95%     0BSD
explicit
        could not clone repo from explicit: repository not found
github.com/Azure/azure-amqp-common-go/v3
        95%     MIT
        82%     MIT-0
github.com/Azure/azure-event-hubs-go/v3
        95%     MIT
        82%     MIT-0
explicit
        could not clone repo from explicit: repository not found
github.com/Azure/azure-pipeline-go
        94%     MIT
        81%     MIT-0
github.com/Azure/azure-sdk-for-go
        99%     Apache-2.0
        90%     ECL-2.0
        85%     SHL-0.51
...

and so on




