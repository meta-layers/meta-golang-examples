DESCRIPTION = "golicense"
SUMMARY = "golicense is a tool that scans compiled Go binaries and can output all the dependencies, \
           their versions, and their respective licenses (if known). \
           golicense only works with Go binaries compiled using Go modules for dependency management."
HOMEPAGE = "https://github.com/mitchellh/golicense"

# bitbake github.com-mitchellh-golicense-native -c addto_recipe_sysroot
# oe-run-native github.com-mitchellh-golicense-native golicense -h

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://src/${GO_IMPORT}/LICENSE.txt;md5=41295d51b657fd255b7fcffa4cec6c68"

GO_IMPORT = "github.com/mitchellh/golicense"

# don't check out modules read only - se we can clean/cleanall
GOBUILDFLAGS:append = " -modcacherw"
inherit go go-mod

BRANCH = "nobranch=1"
SRCREV = "8c09a94a11ac73299a72a68a7b41e3a737119f91"

SRC_URI = "\
           git://github.com/mitchellh/golicense;${BRANCH};protocol=https;destsuffix=${BPN}-${PV}/src/${GO_IMPORT} \
          "
